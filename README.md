# README

It is NOT necessary to clone this repository!

Make sure the prerequisites are in place as outlined in this document then execute the `echo curl` command as shown below.

# AlertHub Local Environment Setup

Setup a Mac workstation for Alert Hub development

## Run the following command from your home directory

`echo "$(curl -fsSL -u 'BITBUCKET_USER:PASSWORD' https://bitbucket.org/superiortechnologies/ah-setup/raw/master/web-install)" | bash -s`

Upon completion, the Alert Hub repositories will be cloned into `$HOME/service-express/alert-hub`

## Starting Alert-Hub

```
cd $HOME/service-express/alert-hub
./start
```

# Prerequisites before you can run the above command

## WorkStation Installations

*NOTE:* The installation script will search for and automatically install the following
applications if they are not already installed:  homebrew, git, nodejs, powershell, golang

### F5 VPN Client

1. Install F5 VPN Client
1. Connect F5 VPN Client to access.seitrakker.com

### Configure NPM to Point to AlertHub Package Registry

1. Create Personal Access Token (PAT) on Gitlab
    1. Navigate to code.serviceexpress.com
    1. Click the user icon in the top right corner
    1. Click “Edit profile”
    1. On the left side of the edit profile view, click “Access Tokens”
    1. Fill out the Name field (“AlertHub is fine”)
    1. Leave the “Expires at” field blank
    1. Check the boxes “read_user”, “read_api”, “read_repository”, “read_registry”
        1. Note: Making a key for each of these fields is possible but for the sake of simplicity one can be made with multiple permissions
    1. Click “Create personal access token”
    1. Copy the personal access token in the box at the top of the view, if this key gets lost another must be generated
1. Navigate to $HOME
1. Open the file .npmrc, if the file does not exist, create it
    1. touch .npmrc
1. Paste these registry references with populated token into the .npmrc file:


```
@alert-hub:registry=https://code.serviceexpress.com/api/v4/packages/npm/
//code.serviceexpress.com/api/v4/packages/npm/:_authToken=<YOUR AUTHTOKEN>
//code.serviceexpress.com/api/v4/projects/271/packages/npm/:<YOUR AUTHTOKEN>
```

### Configure GITLAB, GIT, & GO to Use SSH (Docs)

1. Navigate to ~/.ssh
1. Generate SSH key with the command  ssh-keygen -t ed25519 and follow the prompts
1. There should be 2 keys, id_ed25519 and id_ed25519.pub
1. Go into GitLab and create an SSH connection with the copied key
1. Open ~/.gitconfig
1. Paste contents into .gitconfig to convert GO GIT calls from HTTPS to SSH (explained why in docs in the section header):


```
[user]
name = <YOUR USERNAME>
email = <YOUR EMAIL>@serviceexpress.com

[url "git@code.serviceexpress.com:"]
insteadOf = https://code.serviceexpress.com/
```

### Clone the AlertHub Services
_You probably already have this access if you're reading this README.md_
1. Create bitbucket account with Superior Tech email
1. Get access to the ah-setup project from Jake
1. You are now ready to install AlertHub using the curl command above

