#!/bin/bash
#
# set up alert-hub development repositories
#
# Note: On macOS this script should be located in $HOME/go/src
# Child directories for various git repositories will be created
# under it.
#
# Warning: Existing child directories will be deleted and replaced
# by the contents of the git repositories.
#
# Author:   R. Sweet    rob@superiortech.io
# Date:     4/09/2021
#
# Revision history
# 4/09/2021 R. Sweet    Initial version
# 4/15/2021 R. Sweet    Added sanity check for connectivity (vpn down?)
#                       Sanity check that golang is installed
#                       Sanity check that poweshell is installed
#                       Backup existing local repos if they exist
# 4/26/2021 R. Sweet    Added several additional repositories
# 9/08/2021 R. Sweet    Started mods to load entire alert-hub project
# 9/10/2021 R. Sweet    Finished update to fully support automated install
#
# DEPENDENCIES:
#   An active VPN connection to SEI
#   An account with SSH credentials registered with SEI's GITLAB
#
#   Not a prerequisite for this script but you almost certainly want
#   Visual Studio Code installed as well
#       https://code.visualstudio.com/download

# ===================================================================
# VARIABLES
# ===================================================================

GITLAB="code.serviceexpress.com"
REPOS=( "ontology" "slow-fire-sql" "slow-fire-server" "goapp-dal" "goapp-dto" "adhoc-api" "goapp-gen" "go-build" "go-convert" "go-dynamic" "go-sqladapter" "go-util" "js-api-client" "jsapp-email-sink" "jsapp-rule-processor" "js-expressions" "js-util" "jsvue-ui" )    # all the repos to clone
BASEDIR="$HOME/service-express/alert-hub"   # parent of all alert-hub repositories
INSTDIR="$HOME/service-express/ah-setup"    # location of this setup file

# ===================================================================
# FUNCTIONS
# ===================================================================

# -------------------------------------------------------------------
# clones a project from sei's gitlab and pulls down dependencies
function clone {
    local P=$1

    cd "$BASEDIR"

    # delete existing repo if it exists
    if [[ -d $P ]]
    then
        rm -rf "$P"                 # delete it
    fi

    # clone project
    echo
    if ! git clone "git@$GITLAB:alert-hub/${P}.git"
    then
        echo
        echo "git clone failed for $P"
        echo
        exit 1
    fi

    cd "$BASEDIR"
}

# ===================================================================
# MAIN
# ===================================================================

# -------------------------------------------------------------------
# Sanity checks

echo "Performing sanity checks ... "

# Make sure vpn is active
if ! ping -c1 -W1 "$GITLAB" &> /dev/null
then
    echo
    echo "***********************************************************"
    echo "Unable to reach remote repository: $GITLAB !"
    echo "Is VPN down?"
    echo "Aborting"
    echo "***********************************************************"
    echo
    exit 1
fi

# make sure that $BASEDIR exists
if [ ! -d "$BASEDIR" ]; then 
    echo "$BASEDIR not found. Creating ..."
    mkdir -p "$BASEDIR"
    cd "$BASEDIR"
fi

if ! brew --version &> /dev/null
then
    echo "Homebrew not detected (seriously?), installing ..."
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

if ! git --version &> /dev/null
then
    echo "git not detected, installing ..."
    brew install git
fi

# make sure golang is installed
if ! go version &> /dev/null
then
    echo "Golang not detected, installing ..."
    brew install golang
fi

# make sure nodejs is installed
if ! node --version &> /dev/null
then
    echo "Node.js not detected, installing ..."
    brew install nodejs
fi

# make sure powershell is installed
if ! pwsh --version &> /dev/null
then
    echo "Powershell not detected, installing ..."
    brew install --cask powershell
fi

# make sure that test.env exists in this directory
if [[ ! -f "$BASEDIR/test.env" ]]; then
    echo "Generating test.env"
cat >"$BASEDIR/test.env" <<_TESTENV
DB_HOST=UATSQLListener1.grandrapids.seiservice.com:1433
DB_NAME=slow-fire-ci
DB_USER=slowfire_user
DB_PASS=Lx43itp9E5vGvKf
_TESTENV
fi

echo "Passed all sanity checks"

# -------------------------------------------------------------------
# clone repos

cd "$BASEDIR" 

echo "---------------------------------"
echo "Pulling down project repositories"
for repo in "${REPOS[@]}"
do
    clone "$repo"   # main project
done

# update jsvue-ui/package.json
sed -i -e 's/api-client":.*/api-client": "dev",/' "$BASEDIR/jsvue-ui/package.json"
cd "$BASEDIR"

# run npm install on several repos
echo "---------------------------------"
echo "Making sure we have the latest npm"
npm install -g npm@latest
echo "---------------------------------"
echo "Running npm"
echo jsapp-rule-processor jsvue-ui jsapp-email-sink | xargs -n 1 -I % sh -c "cd % ; npm --loglevel=error install"

echo "---------------------------------"
echo "Pulling down dependencies for slow-fire-server"
cd "$BASEDIR/slow-fire-server"
go get -v ./...

echo "---------------------------------"
echo "Configuring DAL"
cd "$BASEDIR/goapp-dal"
go get -v -tags tools ./...
pwsh ./update-tools.ps1

# this is a bit of a kludge, as the perms are wrong on a file and I don't want to mess with the official repo
find "$BASEDIR" -name '*.sh' -exec chmod +x {} \;

# now that we're all set up run tests
if [ "1" = "2" ]; then
    echo "---------------------------------"
    echo "Running tests"
    pwsh ./test.ps1 -Db -Model -View -Verbose -Clean -Measure
fi

# install env files

echo "---------------------------------"
echo "Setting up environment"
for repo in jsapp-email-sink jsapp-rule-processor jsvue-ui slow-fire-server
do
    SRC="$INSTDIR/${repo}.env"
    DST="$BASEDIR/$repo/config"
    if [ -e $SRC ]; then
        mkdir -p "$DST" || echo "ERROR: Unable to create dir $DST"
        cp "$SRC" "$DST/local.env" || echo "ERROR: Unable to copy $SRC to $DST"
    else
        echo "***********************************************************"
        echo "ERROR: $SRC not found!"
        echo "***********************************************************"
    fi
done

echo "---------------------------------"
echo "Copying over start scripts"
cp $INSTDIR/start* "$BASEDIR"
echo

echo "---------------------------------"
echo "Cleaning up"
cd "$BASEDIR"
cd ..
echo '$ rm -rf "$INSTDIR"'
echo "---------------------------------"
echo "Setup complete"
echo
echo "To start local servers ..."
echo
echo "cd $BASEDIR"
echo "./start"
echo

exit 0
